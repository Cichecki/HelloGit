package pl.sda.hello;

import java.util.stream.IntStream;

public class Hello {

	public static void main(String[] args) {
		System.out.println("Hello GIT");
		System.out.println("new line");
		for (int i = 0; i < 3; i++) {
			System.out.println(i);
		}
		
		Numbers numbers = new Numbers();
		System.out.println(numbers.giveIntegers());
		System.out.println(IntStream.of(4,3,2,5,7,4,3,2).average().getAsDouble());
		
		System.out.println("Next text");
	}

}
